package com.dseer.client;


import lombok.AllArgsConstructor;
import okhttp3.HttpUrl;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@AllArgsConstructor
public class UrlFormatter {
    private final String url;

    public static UrlFormatter create(final String url) {
        return new UrlFormatter(url);
    }

    public HttpUrl prepareUrl(final String endpoint,
                       final Map<String, String> uriVariables,
                       final Map<String, List<String>> queryVariables) {
        final StringBuilder builder = new StringBuilder()
                .append(url)
                .append(endpoint);

        uriVariables.forEach((urlVariable, value) -> builder.replace(
                builder.indexOf(urlVariable),
                builder.indexOf(urlVariable) + urlVariable.length(),
                value)
        );

        final HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(builder.toString()))
                .newBuilder();

        queryVariables.forEach((key, values) -> values.forEach(value -> urlBuilder.addQueryParameter(key, value)));
        return urlBuilder.build();
    }
}
