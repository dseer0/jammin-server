package com.dseer.client.user;

import com.dseer.api.room.RoomCreateApi;
import com.dseer.api.room.RoomDTO;
import com.dseer.api.room.RoomError;
import com.dseer.api.room.RoomMapping;
import com.dseer.api.user.UserCreateApi;
import com.dseer.api.user.UserDTO;
import com.dseer.api.user.UserError;
import com.dseer.api.user.UserMapping;
import com.dseer.client.UrlFormatter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.collection.List;
import io.vavr.control.Either;
import io.vavr.jackson.datatype.VavrModule;
import okhttp3.*;

import java.util.Map;

public class RoomClient {
    OkHttpClient client = new OkHttpClient();
    private final String endpoint;
    private final UrlFormatter urlFormatter;

    public RoomClient(String host, int port) {
        endpoint = String.format("http://%s:%d", host, port);
        urlFormatter = UrlFormatter.create(endpoint);
    }

    public Either<RoomError, RoomDTO> createRoom(RoomCreateApi roomCreateApi) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new VavrModule());

        try {
            //FIXME po co tutaj ten application/json, mediatype? bez tego nie dziala
            RequestBody requestBody = RequestBody.create(objectMapper.writeValueAsBytes(roomCreateApi), MediaType.get("application/json"));
            HttpUrl preparedUrl = urlFormatter.prepareUrl(RoomMapping.CREATE_ROOM, Map.of(), Map.of());


            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(preparedUrl)
                    .build();

            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return Either.right(objectMapper.readValue(response.body().string(), RoomDTO.class));
            } else {
                return Either.left(objectMapper.readValue(response.body().string(), RoomError.class));
            }
        } catch (Exception e) {
            //TODO bez ekszeptiona sie da na pewnoi
            System.out.println(e.getMessage());
            return Either.left(RoomError.ROOM_EXISTS);
        }
    }

    public List<RoomDTO> getAllRooms() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new VavrModule());

        try {
            HttpUrl preparedUrl = urlFormatter.prepareUrl(RoomMapping.API, Map.of(), Map.of());


            Request request = new Request.Builder()
                    .get()
                    .url(preparedUrl)
                    .build();

            Response response = client.newCall(request).execute();

            List<RoomDTO> rooms = objectMapper.readValue(response.body().string(), new TypeReference<List<RoomDTO>>() {
            });
            return rooms;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return List.empty();
        }
    }

    public Either<RoomError, RoomDTO> joinRoom(String room, String user) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new VavrModule());

        try {
            //FIXME po co tutaj ten application/json, mediatype? bez tego nie dziala
            HttpUrl preparedUrl = urlFormatter.prepareUrl(RoomMapping.JOIN_ROOM, Map.of(RoomMapping.NAME, room, RoomMapping.USERNAME, user), Map.of());


            Request request = new Request.Builder()
                    .post(RequestBody.create("", null))
                    .url(preparedUrl)
                    .build();

            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                return Either.right(objectMapper.readValue(response.body().string(), RoomDTO.class));
            } else {
                return Either.left(objectMapper.readValue(response.body().string(), RoomError.class));
            }
        } catch (Exception e) {
            //TODO bez ekszeptiona sie da na pewnoi
            System.out.println("exception" + e.getMessage());
            return Either.left(RoomError.ROOM_EXISTS);
        }
    }
}
