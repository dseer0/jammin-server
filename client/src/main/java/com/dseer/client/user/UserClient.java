package com.dseer.client.user;

import com.dseer.api.user.UserCreateApi;
import com.dseer.api.user.UserDTO;
import com.dseer.api.user.UserError;
import com.dseer.api.user.UserMapping;
import com.dseer.client.UrlFormatter;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Either;
import okhttp3.*;

import java.util.Map;

public class UserClient {
    OkHttpClient client = new OkHttpClient();
    private final String endpoint;
    private final UrlFormatter urlFormatter;

    public UserClient(String host, int port) {
        endpoint = String.format("http://%s:%d", host, port);
        urlFormatter = UrlFormatter.create(endpoint);
    }

    public Either<UserError, UserDTO> getUser(final String nick) {

        HttpUrl preparedUrl = urlFormatter.prepareUrl(UserMapping.BY_NICK, Map.of(UserMapping.NICK, nick), Map.of());

        Request request = new Request.Builder()
                .get()
                .url(preparedUrl)
                .build();
        //TODO tu moga byc jakies bledy
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Response response = client.newCall(request).execute();
            if (response.isSuccessful())
                return Either.right(objectMapper.readValue(response.body().string(), UserDTO.class));
            else return Either.left(objectMapper.readValue(response.body().string(), UserError.class));
        } catch (Exception e) {
            System.out.println("ex-> " + e.getMessage());
            return Either.left(UserError.NOT_FOUND);
        }
    }

    public Either<UserError, UserDTO> createUser(UserCreateApi userCreateApi) {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            //FIXME po co tutaj ten application/json, mediatype? bez tego nie dziala
            RequestBody requestBody = RequestBody.create(objectMapper.writeValueAsBytes(userCreateApi), MediaType.get("application/json"));
            HttpUrl preparedUrl = urlFormatter.prepareUrl(UserMapping.CREATE_USER, Map.of(), Map.of());


            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(preparedUrl)
                    .build();

            Response response = client.newCall(request).execute();

//
//            //FIXME jak tutaj uzyje objetMapper to psuje sie test, czemu?
//            System.out.println("body-> " + response.body().string());

            if (response.isSuccessful()) {
                return Either.right(objectMapper.readValue(response.body().string(), UserDTO.class));
            } else {
                return Either.left(objectMapper.readValue(response.body().string(), UserError.class));
            }
        } catch (Exception e) {
            //TODO bez ekszeptiona sie da na pewnoi
            System.out.println(e.getMessage());
            return Either.left(UserError.USERNAME_EXISTS);
        }
    }
}
