package com.dseer.model.room;

import com.dseer.api.room.RoomDTO;
import com.dseer.model.user.UserMapper;

import java.util.stream.Collectors;

public class RoomMapper {

    public static RoomDTO toDTO(Room room) {
        return new RoomDTO(room.getName(), room.getMaxUsers(), room.getUsers().stream().map(UserMapper::toDto).collect(Collectors.toSet()));
    }
}
