package com.dseer.model.room;

import com.dseer.model.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
@Table(name = "ROOMS")
@Entity
@Getter
public class Room {
    @Id
    private String name;
    private Integer maxUsers;
    //TODO tutaj jakies to cos tam c nie?
    @OneToMany(
            mappedBy = "room",
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private List<User> users;

    public Room() {
    }

    public static Room createNewRoom(final String name, final Integer maxUsers) {
        return new Room(name, maxUsers, new LinkedList<>());
    }

    public void addUser(User user) {
        users.add(user);
        user.setRoom(this);
    }

    public void removeUser(User user) {
        users.remove(user);
        user.setRoom(null);
    }

}
