package com.dseer.model.user;

import com.dseer.model.room.Room;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@AllArgsConstructor
@Table(name = "USERS")
@Getter
public class User {
    @Id
    private String nickname;
    private String ip;
    private LocalDateTime heartbeat;
    //TODO ogarnac czy tutaj moze byc option albo cos takiego
    //TODO tutaj mapowanie
    //FIXME TE EAGER
    @ManyToOne(fetch = FetchType.EAGER)
    private Room room;

    public User() {

    }



    public String getNickname() {
        return nickname;
    }

    public String getIp() {
        return ip;
    }

    public LocalDateTime getHeartbeat() {
        return heartbeat;
    }

    public Option<Room> getRoom() {
        return Option.of(room);
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setHeartbeat(LocalDateTime heartbeat) {
        this.heartbeat = heartbeat;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}

