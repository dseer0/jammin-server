package com.dseer.model.user;

import com.dseer.api.user.UserDTO;
public class UserMapper {

    public static UserDTO toDto(User user) {
        return new UserDTO(user.getNickname());
    }

}
