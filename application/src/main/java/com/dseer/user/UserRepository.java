package com.dseer.user;

import com.dseer.api.user.UserError;
import com.dseer.model.user.User;
import io.vavr.control.Either;
import io.vavr.control.Option;


public interface UserRepository {
    default Either<UserError, User> loadOrError(final String nick) {
        return load(nick)
                .toEither(UserError.NOT_FOUND);
    }

    Option<User> load(String nick);

    User save(final User user);
}
