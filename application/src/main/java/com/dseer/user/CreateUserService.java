package com.dseer.user;

import com.dseer.api.user.UserCreateApi;
import com.dseer.api.user.UserDTO;
import com.dseer.api.user.UserError;
import com.dseer.model.user.User;
import com.dseer.model.user.UserMapper;
import io.vavr.control.Either;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
public class CreateUserService {
    public final UserRepository userRepository;


    public Either<UserError, UserDTO> createUser(final UserCreateApi userCreateApi) {
        //TODO check czy jest taki nickname itd.
        //FIXME to nie dziala bez transaction bo raceCondition, ale poczytac o transaction jak to dziala dokladnie

        if (userRepository.load(userCreateApi.nickname).isDefined()) {
            return Either.left(UserError.USERNAME_EXISTS);
        }
        return Either.right(
                UserMapper.toDto(
                        userRepository.save(new User(userCreateApi.nickname, userCreateApi.ip, LocalDateTime.now(), null
                        ))));
    }
}
