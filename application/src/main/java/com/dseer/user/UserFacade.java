package com.dseer.user;

import com.dseer.api.user.UserCreateApi;
import com.dseer.api.user.UserDTO;
import com.dseer.api.user.UserError;
import com.dseer.model.user.UserMapper;
import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import lombok.Value;

@AllArgsConstructor
@Value
public class UserFacade {
    UserRepository userRepository;
    CreateUserService createUserService;

    public Either<UserError, UserDTO> getUserByNick(final String nick) {
        return userRepository.loadOrError(nick)
                .map(UserMapper::toDto);
    }

    public Either<UserError, UserDTO> createUser(final UserCreateApi userCreateApi) {
        return createUserService.createUser(userCreateApi);
    }
}
