package com.dseer.room;

import com.dseer.api.room.RoomDTO;
import com.dseer.api.room.RoomError;
import io.vavr.collection.List;
import io.vavr.control.Either;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RoomFacade {
    private final CreateRoomService createRoomService;
    private final RoomGetAllService roomGetAllService;
    private final JoinRoomService joinRoomService;

    public Either<RoomError, RoomDTO> createRoom(String name, Integer maxUsers) {
        return createRoomService.createRoom(name, maxUsers);
    }

    public Either<RoomError, RoomDTO> joinRoom(String room, String username) {
        return joinRoomService.join(room, username);
    }

    public List<RoomDTO> getAll() {
        return roomGetAllService.getAll();
    }
}
