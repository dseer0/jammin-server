package com.dseer.room;

import com.dseer.api.room.RoomDTO;
import com.dseer.api.room.RoomError;
import com.dseer.model.room.Room;
import com.dseer.model.room.RoomMapper;
import io.vavr.control.Either;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CreateRoomService {
    private final RoomRepository roomRepository;


    public Either<RoomError, RoomDTO> createRoom(String name, Integer maxUsers) {
        if(roomRepository.load(name).isDefined()) return Either.left(RoomError.ROOM_EXISTS);

        return Either.right(RoomMapper.toDTO(roomRepository.save(Room.createNewRoom(name, maxUsers))));
    }
}
