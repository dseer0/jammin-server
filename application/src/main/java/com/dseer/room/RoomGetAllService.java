package com.dseer.room;


import com.dseer.api.room.RoomDTO;
import com.dseer.model.room.RoomMapper;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RoomGetAllService {
    private final RoomRepository roomRepository;

    List<RoomDTO> getAll() {
        return roomRepository.getAll().map(RoomMapper::toDTO);
    }
}
