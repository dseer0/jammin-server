package com.dseer.room;


import com.dseer.api.room.RoomDTO;
import com.dseer.api.room.RoomError;
import com.dseer.model.room.Room;
import com.dseer.model.room.RoomMapper;
import com.dseer.model.user.User;
import com.dseer.user.UserRepository;
import io.vavr.control.Either;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class JoinRoomService {
    private final RoomRepository roomRepository;
    private final UserRepository userRepository;

    public Either<RoomError, RoomDTO> join(String room, String username) {
        Option<Room> roomToJoin = roomRepository.load(room);
        Option<User> user = userRepository.load(username);
        if(!roomToJoin.isDefined()) return Either.left(RoomError.NOT_FOUND);
        if(!user.isDefined()) return Either.left(RoomError.NOT_FOUND); //FIXME
        return roomToJoin.filter(room1 -> room1.getUsers().size() < room1.getMaxUsers())
                .peek(room1 -> room1.addUser(user.get()))
                .peek(roomRepository::save)
                .map(RoomMapper::toDTO)
                .toEither(RoomError.FULL);
    }
}
