package com.dseer.room;

import com.dseer.api.room.RoomError;
import com.dseer.model.room.Room;
import io.vavr.collection.List;
import io.vavr.control.Either;
import io.vavr.control.Option;

public interface RoomRepository {
    default Either<RoomError, Room> loadOrError(final String name) {
        return load(name)
                .toEither(RoomError.NOT_FOUND);
    }

    Option<Room> load(String name);

    Room save(final Room room);

    List<Room> getAll();
}
