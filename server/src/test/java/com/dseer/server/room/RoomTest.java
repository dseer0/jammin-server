package com.dseer.server.room;

import com.dseer.api.room.RoomCreateApi;
import com.dseer.api.room.RoomDTO;
import com.dseer.api.room.RoomError;
import com.dseer.api.user.UserCreateApi;
import com.dseer.api.user.UserDTO;
import com.dseer.api.user.UserError;
import com.dseer.client.user.RoomClient;
import com.dseer.client.user.UserClient;
import com.dseer.server.BaseTest;
import io.vavr.control.Either;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import javax.validation.constraints.AssertTrue;

import static org.junit.Assert.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RoomTest extends BaseTest {

    @Test
    public void createSameRoomTwiceShouldntPass() {
        Either<RoomError, RoomDTO> room = roomClient.createRoom(new RoomCreateApi("room1", 6));
        Either<RoomError, RoomDTO> sameRoom = roomClient.createRoom(new RoomCreateApi("room1", 36));

        assertTrue(room.isRight());
        assertTrue(sameRoom.isLeft());
        assertTrue(sameRoom.getLeft().equals(RoomError.ROOM_EXISTS));
    }

    @Test
    public void createBunchOfRooms() {
        Either<RoomError, RoomDTO> room = roomClient.createRoom(new RoomCreateApi("room1", 6));
        Either<RoomError, RoomDTO> room2 = roomClient.createRoom(new RoomCreateApi("room2", 36));
        Either<RoomError, RoomDTO> room3 = roomClient.createRoom(new RoomCreateApi("room3", 6));
        Either<RoomError, RoomDTO> room4 = roomClient.createRoom(new RoomCreateApi("room4", 36));


        assertTrue(roomClient.getAllRooms().size() == 4);
    }

    @Test
    public void createBunchOfRooms2() {
        Either<RoomError, RoomDTO> room = roomClient.createRoom(new RoomCreateApi("room1", 6));
        Either<RoomError, RoomDTO> room2 = roomClient.createRoom(new RoomCreateApi("room2", 36));

        assertTrue(roomClient.getAllRooms().size() == 2);
    }

    @Test
    public void joiningRoom() {
        Either<UserError, UserDTO> user = userClient.createUser(new UserCreateApi("willJoin", "3"));
        Either<RoomError, RoomDTO> room = roomClient.createRoom(new RoomCreateApi("roomToJoin", 6));
        System.out.println(user.toString());
        System.out.println(room.toString());

        Either<RoomError, RoomDTO> joinresponse = roomClient.joinRoom(room.get().name, user.get().nickname);


        assertTrue(user.isRight());
        assertTrue(room.isRight());


        assertTrue(joinresponse.isRight());
        assertTrue(joinresponse.get().users.size() == 1);

    }

    @Test
    public void manyUserJoiningRoom() {
        Either<RoomError, RoomDTO> room = roomClient.createRoom(new RoomCreateApi("roomToJoin", 6));

        Either<UserError, UserDTO> user1 = userClient.createUser(new UserCreateApi("user1", "3"));
        Either<UserError, UserDTO> user2 = userClient.createUser(new UserCreateApi("user2", "3"));
        Either<UserError, UserDTO> user3 = userClient.createUser(new UserCreateApi("user3", "3"));
        Either<UserError, UserDTO> user4 = userClient.createUser(new UserCreateApi("user4", "3"));
        Either<UserError, UserDTO> user5 = userClient.createUser(new UserCreateApi("user5", "3"));

        roomClient.joinRoom(room.get().name, user1.get().nickname);
        roomClient.joinRoom(room.get().name, user2.get().nickname);
        roomClient.joinRoom(room.get().name, user3.get().nickname);
        roomClient.joinRoom(room.get().name, user4.get().nickname);
        Either<RoomError, RoomDTO> result = roomClient.joinRoom(room.get().name, user5.get().nickname);


        assertTrue(result.get().users.size() == 5);
    }
}
