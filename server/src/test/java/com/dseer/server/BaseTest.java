package com.dseer.server;

import com.dseer.client.user.RoomClient;
import com.dseer.client.user.UserClient;
import com.dseer.user.UserRepository;
import io.vavr.control.Option;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import javax.inject.Inject;

public class BaseTest {

    protected RoomClient roomClient;
    protected UserClient userClient;

    @LocalServerPort
    protected int port;

    @Inject
    private NamedParameterJdbcTemplate as3jdbcTemplate;





    @AfterEach
    public void cleanup() {
        cleanDB();
    }

    @BeforeEach
    public void setup(){
        roomClient = new RoomClient("127.0.0.1", port);
        userClient = new UserClient("127.0.0.1", port);
    }

    //FIXME nie dziala
    public void cleanDB() {
        for (String tableName : Arrays.asList(
                "users",
                "rooms")) {
            as3jdbcTemplate.execute("delete from " + tableName, Collections.emptyMap(), x -> null);
        }
    }
}
