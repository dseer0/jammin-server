package com.dseer.server.user;

import com.dseer.api.user.UserCreateApi;
import com.dseer.api.user.UserDTO;
import com.dseer.api.user.UserError;
import com.dseer.client.user.UserClient;
import com.dseer.server.BaseTest;
import io.vavr.control.Either;
//TODO skąd mam jupitera, chyba z spring boot starter
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import static org.junit.Assert.*;

//8080

//TODO czyszczenie bazy miedzy testami

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserTest extends BaseTest {



    @Test
    public void testGet(){
        Either<UserError, UserDTO> res =  userClient.createUser(new UserCreateApi("ee", "3"));
        Either<UserError, UserDTO> userDto = userClient.getUser("ee");

        assertTrue(res.isRight());
        assertTrue(userDto.isRight());
        assertTrue(userDto.get().nickname.equals("ee"));

    }

    @Test
    public void createSameUserTwiceShouldntPass(){
        Either<UserError, UserDTO> user =  userClient.createUser(new UserCreateApi("nowy", "6"));
        Either<UserError, UserDTO>  sameUser =  userClient.createUser(new UserCreateApi("nowy", "4"));

        assertTrue(user.isRight());
        assertTrue(sameUser.isLeft());
        assertTrue(sameUser.getLeft().equals(UserError.USERNAME_EXISTS));
    }

}
