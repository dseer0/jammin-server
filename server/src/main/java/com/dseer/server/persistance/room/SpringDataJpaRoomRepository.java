package com.dseer.server.persistance.room;

import com.dseer.model.room.Room;
import com.dseer.room.RoomRepository;
import io.vavr.collection.List;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class SpringDataJpaRoomRepository implements RoomRepository {
    private final SpringDataJpaRoomDao dataJpaRoomDao;

    @Override
    public Option<Room> load(String name) {
        return dataJpaRoomDao.getByName(name);
    }

    @Override
    public Room save(Room room) {
        return dataJpaRoomDao.save(room);
    }


    //FIXME to jest wolne?
    @Override
    public List<Room> getAll() {
        return List.ofAll(dataJpaRoomDao.findAll());
    }
}
