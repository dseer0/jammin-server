package com.dseer.server.persistance.room;

import com.dseer.model.room.Room;
import io.vavr.control.Option;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringDataJpaRoomDao extends JpaRepository<Room, String> {
    Option<Room> getByName(String name);
}
