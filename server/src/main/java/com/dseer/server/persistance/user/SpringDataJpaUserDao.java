package com.dseer.server.persistance.user;

import com.dseer.model.user.User;
import io.vavr.control.Option;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringDataJpaUserDao  extends JpaRepository<User, String> {
    Option<User> getByNickname(String nickname);
}
