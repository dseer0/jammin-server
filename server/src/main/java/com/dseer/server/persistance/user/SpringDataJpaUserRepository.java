package com.dseer.server.persistance.user;

import com.dseer.model.user.User;
import com.dseer.user.UserRepository;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;


@Repository
@AllArgsConstructor
public class SpringDataJpaUserRepository implements UserRepository {
    SpringDataJpaUserDao springDataJpaUserDao;

    @Override
    public Option<User> load(String nick) {
        return springDataJpaUserDao.getByNickname(nick);
    }

    @Override
    public User save(User user) {
        return springDataJpaUserDao.saveAndFlush(user);
    }
}
