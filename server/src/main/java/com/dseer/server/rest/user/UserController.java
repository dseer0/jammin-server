package com.dseer.server.rest.user;

import com.dseer.user.UserFacade;
import com.dseer.api.user.UserCreateApi;
import com.dseer.api.user.UserDTO;
import com.dseer.api.user.UserError;
import com.dseer.api.user.UserMapping;
import com.dseer.server.rest.ResponseResolver;
import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
public class UserController {
    private final UserFacade userFacade;



    @GetMapping(UserMapping.BY_NICK)
    public ResponseEntity<?> getUser(@PathVariable final String nick){
        //TODO resolvery
        final Either<UserError, UserDTO> response =  userFacade.getUserByNick(nick);
        return ResponseResolver.resolve(response);
    }

    @PostMapping(UserMapping.CREATE_USER)
    public ResponseEntity<?> createUser(final @RequestBody @Valid UserCreateApi userCreateApi){
        final Either<UserError, UserDTO> response =  userFacade.createUser(userCreateApi);
        return ResponseResolver.resolve(response);
    }


}
