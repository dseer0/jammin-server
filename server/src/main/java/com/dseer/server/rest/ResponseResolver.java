package com.dseer.server.rest;

import com.dseer.api.ErrorResponse;
import com.dseer.api.IResponseError;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseResolver {

    //TODO collection
    public static ResponseEntity resolve(List<?> list){
        return ResponseEntity.ok(list);
    }

    public static ResponseEntity resolve(Either<? extends IResponseError, ?> toResolve){
        return toResolve.map(ResponseEntity::ok)
                .getOrElseGet(ResponseResolver::resolveError);
    }

    private static ResponseEntity resolveError(IResponseError error){
        ErrorResponse response = new ErrorResponse(error.getMessage());
        int httpCode = error.getHttpCode();
        return new ResponseEntity<>(response, HttpStatus.valueOf(httpCode));
    }


}
