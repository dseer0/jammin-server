package com.dseer.server.rest.room;

import com.dseer.api.room.RoomCreateApi;
import com.dseer.api.room.RoomDTO;
import com.dseer.api.room.RoomError;
import com.dseer.api.room.RoomMapping;
import com.dseer.api.user.UserDTO;
import com.dseer.api.user.UserError;
import com.dseer.room.RoomFacade;
import com.dseer.server.rest.ResponseResolver;
import io.vavr.collection.List;
import io.vavr.control.Either;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@RestController
public class RoomController {

    private final RoomFacade roomFacade;

    //TODO create room z autodolaczaniem?
    @PostMapping(RoomMapping.CREATE_ROOM)
    public ResponseEntity<?> createRoom(final @RequestBody @Valid RoomCreateApi roomCreateApi) {
        final Either<RoomError, RoomDTO> response = roomFacade.createRoom(roomCreateApi.name, roomCreateApi.maxUsers);
        return ResponseResolver.resolve(response);
    }

    @GetMapping(RoomMapping.API)
    public ResponseEntity<?> getAll() {
        final List<RoomDTO> rooms = roomFacade.getAll();
        return ResponseResolver.resolve(rooms);
    }

    //TODO security
    @PostMapping(RoomMapping.JOIN_ROOM)
    public ResponseEntity<?> joinRoom(final @PathVariable String name, final @PathVariable String username) {
        final Either<RoomError, RoomDTO> response = roomFacade.joinRoom(name, username);
        return ResponseResolver.resolve(response);
    }

}
