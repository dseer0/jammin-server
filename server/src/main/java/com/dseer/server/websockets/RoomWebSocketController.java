package com.dseer.server.websockets;

import com.dseer.api.room.RoomDTO;
import com.dseer.api.user.UserDTO;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class RoomWebSocketController {

    @MessageMapping("/room/joined/{room}")
    public UserDTO joined(@DestinationVariable String room, UserDTO userDTO) throws Exception {
        return userDTO;
    }
}
