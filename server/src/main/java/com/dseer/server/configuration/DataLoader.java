package com.dseer.server.configuration;

import com.dseer.model.room.Room;
import com.dseer.server.persistance.room.RoomOld;
import com.dseer.room.RoomRepository;
import com.dseer.model.user.User;
import com.dseer.user.UserRepository;
import com.dseer.server.persistance.user.UserOld;
import io.vavr.control.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class DataLoader implements ApplicationRunner {

    private UserRepository userRepository;
    private RoomRepository roomRepository;

    @Autowired
    public DataLoader(UserRepository userRepository, RoomRepository roomRepository) {
        this.userRepository = userRepository;
        this.roomRepository = roomRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        roomRepository.save(new Room("room1", 3, List.of()));
        roomRepository.save(new Room("room2", 2, List.of()));
    }
}
