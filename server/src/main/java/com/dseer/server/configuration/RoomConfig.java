package com.dseer.server.configuration;


import com.dseer.room.*;
import com.dseer.server.persistance.room.SpringDataJpaRoomRepository;
import com.dseer.user.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoomConfig {
    private RoomFacade roomFacade;

    public RoomConfig(RoomRepository roomRepository, UserRepository userRepository) {
        CreateRoomService createRoomService = new CreateRoomService(roomRepository);
        RoomGetAllService roomGetAllService = new RoomGetAllService(roomRepository);
        JoinRoomService joinRoomService = new JoinRoomService(roomRepository, userRepository);
        roomFacade = new RoomFacade(createRoomService, roomGetAllService, joinRoomService);
    }

    @Bean
    RoomFacade roomFacade() {
        return roomFacade;
    }
}
