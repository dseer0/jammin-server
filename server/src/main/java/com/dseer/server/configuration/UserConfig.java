package com.dseer.server.configuration;

import com.dseer.user.CreateUserService;
import com.dseer.user.UserFacade;
import com.dseer.user.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserConfig {

    @Bean
    UserFacade userFacade(UserRepository userRepository){

        CreateUserService createUserService = new CreateUserService(userRepository);
        return new UserFacade(userRepository, createUserService);
    }
}
