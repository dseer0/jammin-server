package com.dseer.api;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ErrorResponse {
    public final String message;
}
