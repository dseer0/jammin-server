package com.dseer.api;

public interface IResponseError {
    String getMessage();

    int getHttpCode();
}
