package com.dseer.api.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;

import java.time.LocalDateTime;

public class UserCreateApi {
    public final String nickname;
    public final String ip;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public UserCreateApi(@JsonProperty("nickname") String nickname, @JsonProperty("ip") String ip) {
        this.nickname = nickname;
        this.ip = ip;
    }
}
