package com.dseer.api.user;

import com.dseer.api.ServerMapping;

public class UserMapping {
    ;
    public static final String API = ServerMapping.API_PREFIX + "/users";

    public static final String CREATE_USER = API;

    public static final String NICK = "{nick}";
    public static final String BY_NICK = API + "/" + NICK;

}
