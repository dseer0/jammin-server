package com.dseer.api.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;


//TODO IP
public class UserDTO {
    public final String nickname;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public UserDTO(@JsonProperty("nickname") String nickname) {
        this.nickname = nickname;
    }
}
