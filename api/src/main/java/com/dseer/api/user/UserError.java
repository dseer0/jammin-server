package com.dseer.api.user;

import com.dseer.api.IResponseError;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public enum UserError implements IResponseError {
    EMPTY_USERNAME_OR_PASSWORD("Empty username", 400),
    USERNAME_EXISTS("username Exists", 400),
    NOT_FOUND("not found", 400);

    private int httpCode;
    private String message;

    UserError(String message, int httpCode) {
        this.httpCode = httpCode;
        this.message = message;
    }

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    static UserError getByMessage(@JsonProperty("message") String message){
        //TODO poprawic
        return Arrays.stream(UserError.values()).filter(userError -> userError.message.equals(message)).findFirst().orElse(UserError.NOT_FOUND);
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public int getHttpCode() {
        return httpCode;
    }
}
