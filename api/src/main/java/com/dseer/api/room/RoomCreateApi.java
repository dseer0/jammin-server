package com.dseer.api.room;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;

public class RoomCreateApi {
    public final String name;
    public final Integer maxUsers;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public RoomCreateApi(@JsonProperty("name") String name, @JsonProperty("maxUsers") Integer maxUsers) {
        this.name = name;
        this.maxUsers = maxUsers;
    }
}
