package com.dseer.api.room;

import com.dseer.api.IResponseError;
import com.dseer.api.user.UserError;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public enum RoomError implements IResponseError {
    ROOM_EXISTS("room exists", 400),
    NOT_FOUND("not found", 400),
    FULL("room is full", 400);

    private int httpCode;
    private String message;

    RoomError(String message, int httpCode) {
        this.httpCode = httpCode;
        this.message = message;
    }

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    static RoomError getByMessage(@JsonProperty("message") String message){
        //TODO poprawic
        return Arrays.stream(RoomError.values()).filter(error -> error.message.equals(message)).findFirst().orElse(RoomError.NOT_FOUND);
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public int getHttpCode() {
        return httpCode;
    }
}
