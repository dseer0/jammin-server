package com.dseer.api.room;

import com.dseer.api.ServerMapping;

public class RoomMapping {
    ;
    public static final String API = ServerMapping.API_PREFIX + "/rooms";

    public static final String CREATE_ROOM = API;

    public static final String NAME = "{name}";
    public static final String USERNAME = "{username}";
    public static final String BY_NAME = API + "/" + NAME;
    public static final String JOIN_ROOM = API + "/" + NAME + "/" + USERNAME;
}
