package com.dseer.api.room;

import com.dseer.api.user.UserDTO;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class RoomDTO {
    public final String name;
    public final Integer maxUsers;
    public final Set<UserDTO> users;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public RoomDTO(@JsonProperty("name") String name, @JsonProperty("maxUsers") Integer maxUsers, @JsonProperty("users") Set<UserDTO> users) {
        this.name = name;
        this.maxUsers = maxUsers;
        this.users = users;
    }
}
